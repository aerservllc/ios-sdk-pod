# ios-sdk-pod -- aerserv-ios-sdk cocoa pods

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

aerserv-ios-sdk is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "aerserv-ios-sdk"
```

## Author

Albert Zhu, albert.zhu@inmobi.com

## License

aerserv-ios-sdk is available under the MIT license. See the LICENSE file for more info.
