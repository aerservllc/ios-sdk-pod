#
# Be sure to run `pod lib lint ios-sdk-pod.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             	= "aerserv-ios-sdk"
  s.version             = "3.1.3.rc5"
  s.summary             = "AerServ-iOS-SDK Pod"
  s.description         = "AerServ-iOS-SDK's CocoaPods Integration"
  s.homepage         	= "https://bitbucket.org/aerservllc/ios-sdk-pod"
  s.license          	= 'Copyright 2013-2018 AerServ-InMobi, all rights reserved'
  s.author           	= { "Albert Zhu" => "albert.zhu@inmobi.com" }
  s.source           	= { :git => "https://bitbucket.org/aerservllc/ios-sdk-pod.git", :tag => s.version }

  s.platform     	= :ios, '8.0'
  s.ios.framework   	= 'WebKit'
  s.ios.library		= 'xml2.2'
  s.xcconfig		= { 'OTHER_LDFLAGS' => '-ObjC', 'HEADER_SEARCH_PATHS' => '$(SDKROOT)/usr/include/libxml2' }

  s.source_files = 'Frameworks/AerServSDK.framework/Headers/*.h'
  s.ios.public_header_files = 'Frameworks/AerServSDK.framework/Headers/*.h'
  s.vendored_frameworks = 'Frameworks/AerServSDK.framework'
end

